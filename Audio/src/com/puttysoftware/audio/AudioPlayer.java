package com.puttysoftware.audio;

public abstract class AudioPlayer {
    public abstract boolean isPlaying();

    public abstract void play();

    public abstract void syncPlay();

    public abstract void playLoop();

    public abstract void stopLoop();
}
