package com.puttysoftware.audio;

import java.io.IOException;
import java.net.URL;

import com.puttysoftware.audio.midi.MIDIPlayer;
import com.puttysoftware.audio.mod.ModPlayer;
import com.puttysoftware.audio.ogg.OggPlayer;
import com.puttysoftware.audio.wav.WAVPlayer;

public class AudioPlayerFactory {
    public static AudioPlayer getPlayerForURL(final URL audioFile) throws IOException {
	final String ext = AudioPlayerFactory.getExtension(audioFile.toExternalForm().toLowerCase());
	if ("mod".equals(ext) || "s3m".equals(ext) || "xm".equals(ext)) { //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
	    return new ModPlayer(audioFile);
	} else if ("ogg".equals(ext)) { //$NON-NLS-1$
	    return new OggPlayer(audioFile);
	} else if ("wav".equals(ext)) { //$NON-NLS-1$
	    return new WAVPlayer(audioFile);
	} else if ("mid".equals(ext)) { //$NON-NLS-1$
	    return new MIDIPlayer(audioFile);
	} else {
	    throw new IOException("Unsupported audio format!"); //$NON-NLS-1$
	}
    }

    private static String getExtension(final String s) {
	String ext = ""; //$NON-NLS-1$
	final int i = s.lastIndexOf('.');
	if (i > 0 && i < s.length() - 1) {
	    ext = s.substring(i + 1).toLowerCase();
	}
	return ext;
    }
}
