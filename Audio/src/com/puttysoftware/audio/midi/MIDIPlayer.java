package com.puttysoftware.audio.midi;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;

import javax.sound.midi.InvalidMidiDataException;
import javax.sound.midi.MetaEventListener;
import javax.sound.midi.MetaMessage;
import javax.sound.midi.MidiSystem;
import javax.sound.midi.MidiUnavailableException;
import javax.sound.midi.Sequencer;

import com.puttysoftware.audio.AudioPlayer;

public class MIDIPlayer extends AudioPlayer {
    // Fields
    Sequencer sequencer;
    URL soundURL;
    InputStream soundStream;
    Thread player;
    boolean stop;

    // Constructor
    public MIDIPlayer(final URL resource) {
	super();
	this.soundURL = resource;
	this.stop = false;
    }

    // Methods
    @Override
    public void play() {
	this.stop = false;
	this.player = new Thread() {
	    @Override
	    public void run() {
		MIDIPlayer.this.syncPlay();
	    }
	};
	this.player.start();
    }

    @Override
    public void playLoop() {
	this.stop = false;
	this.player = new Thread() {
	    @Override
	    public void run() {
		while (!MIDIPlayer.this.stop) {
		    MIDIPlayer.this.syncPlay();
		}
	    }
	};
	this.player.start();
    }

    @Override
    public void stopLoop() {
	this.stop = true;
	try {
	    if (this.sequencer != null) {
		this.sequencer.close();
	    }
	    this.soundStream.close();
	} catch (final IOException e) {
	    // Ignore
	}
    }

    @Override
    public void syncPlay() {
	try {
	    this.soundStream = this.soundURL.openStream();
	    this.sequencer = MidiSystem.getSequencer();
	    final Sequencer seq2 = this.sequencer;
	    if (seq2 != null) {
		seq2.open();
		seq2.setSequence(this.soundStream);
		seq2.start();
		seq2.addMetaEventListener(new MetaEventListener() {
		    @Override
		    public void meta(MetaMessage evt) {
			if (evt != null && evt.getType() == 47) {
			    try {
				final Sequencer seq3 = MIDIPlayer.this.sequencer;
				if (seq3 != null) {
				    seq3.close();
				}
				MIDIPlayer.this.soundStream.close();
			    } catch (final IOException e) {
				// Ignore
			    }
			}
		    }
		});
	    }
	} catch (MidiUnavailableException | InvalidMidiDataException | IOException e) {
	    // Ignore
	}
    }

    @Override
    public boolean isPlaying() {
	return this.player != null && this.player.isAlive();
    }
}
