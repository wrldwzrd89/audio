package com.puttysoftware.audio.mod;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;

import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.SourceDataLine;

import com.puttysoftware.audio.AudioPlayer;

public class ModPlayer extends AudioPlayer {
    private static final int SAMPLE_RATE = 41000;
    private Module module;
    IBXM ibxm;
    volatile boolean playing;
    private int interpolation;
    private Thread player;

    public ModPlayer(final URL modURL) throws IOException {
	try (final InputStream modFile = modURL.openStream()) {
	    final ByteArrayOutputStream baos = new ByteArrayOutputStream();
	    final byte[] buf = new byte[1024];
	    int bytesRead = 1;
	    while (bytesRead > 0) {
		bytesRead = modFile.read(buf, 0, buf.length);
		if (bytesRead > 0) {
		    baos.write(buf, 0, bytesRead);
		}
	    }
	    final byte[] moduleData = baos.toByteArray();
	    this.module = new Module(moduleData);
	    this.ibxm = new IBXM(this.module, ModPlayer.SAMPLE_RATE);
	    this.ibxm.setInterpolation(this.interpolation);
	}
    }

    public ModPlayer(final File modFile) throws IOException {
	final byte[] moduleData = new byte[(int) modFile.length()];
	try (FileInputStream inputStream = new FileInputStream(modFile)) {
	    int offset = 0;
	    while (offset < moduleData.length) {
		final int len = inputStream.read(moduleData, offset, moduleData.length - offset);
		if (len < 0) {
		    inputStream.close();
		    throw new IOException("Unexpected end of file."); //$NON-NLS-1$
		}
		offset += len;
	    }
	    inputStream.close();
	    this.module = new Module(moduleData);
	    this.ibxm = new IBXM(this.module, ModPlayer.SAMPLE_RATE);
	    this.ibxm.setInterpolation(this.interpolation);
	}
    }

    public ModPlayer(final FileInputStream modFile) throws IOException {
	final byte[] moduleData = new byte[(int) modFile.getChannel().size()];
	int offset = 0;
	while (offset < moduleData.length) {
	    final int len = modFile.read(moduleData, offset, moduleData.length - offset);
	    if (len < 0) {
		throw new IOException("Unexpected end of file."); //$NON-NLS-1$
	    }
	    offset += len;
	}
	this.module = new Module(moduleData);
	this.ibxm = new IBXM(this.module, ModPlayer.SAMPLE_RATE);
	this.ibxm.setInterpolation(this.interpolation);
    }

    @Override
    public boolean isPlaying() {
	return this.player != null && this.player.isAlive();
    }

    @Override
    public void syncPlay() {
	final int[] mixBuf = new int[ModPlayer.this.ibxm.getMixBufferLength()];
	final byte[] outBuf = new byte[mixBuf.length * 4];
	AudioFormat audioFormat = null;
	audioFormat = new AudioFormat(ModPlayer.SAMPLE_RATE, 16, 2, true, true);
	try (SourceDataLine audioLine = AudioSystem.getSourceDataLine(audioFormat)) {
	    audioLine.open();
	    audioLine.start();
	    this.playInternal(mixBuf, outBuf, audioLine);
	    audioLine.drain();
	} catch (final Exception e) {
	    // Ignore
	}
    }

    @Override
    public void play() {
	if (this.ibxm != null) {
	    this.playing = true;
	    this.player = new Thread(new Runnable() {
		@Override
		public void run() {
		    final int[] mixBuf = new int[ModPlayer.this.ibxm.getMixBufferLength()];
		    final byte[] outBuf = new byte[mixBuf.length * 4];
		    AudioFormat audioFormat = null;
		    audioFormat = new AudioFormat(ModPlayer.SAMPLE_RATE, 16, 2, true, true);
		    try (SourceDataLine audioLine = AudioSystem.getSourceDataLine(audioFormat)) {
			audioLine.open();
			audioLine.start();
			ModPlayer.this.playInternal(mixBuf, outBuf, audioLine);
			audioLine.drain();
		    } catch (final Exception e) {
			// Ignore
		    }
		}
	    });
	    this.player.start();
	}
    }

    @Override
    public void playLoop() {
	if (this.ibxm != null) {
	    this.playing = true;
	    this.player = new Thread(new Runnable() {
		@Override
		public void run() {
		    final int[] mixBuf = new int[ModPlayer.this.ibxm.getMixBufferLength()];
		    final byte[] outBuf = new byte[mixBuf.length * 4];
		    AudioFormat audioFormat = null;
		    audioFormat = new AudioFormat(ModPlayer.SAMPLE_RATE, 16, 2, true, true);
		    try (SourceDataLine audioLine = AudioSystem.getSourceDataLine(audioFormat)) {
			audioLine.open();
			audioLine.start();
			while (ModPlayer.this.playing) {
			    ModPlayer.this.playInternal(mixBuf, outBuf, audioLine);
			}
			audioLine.drain();
		    } catch (final Exception e) {
			// Ignore
		    }
		}
	    });
	    this.player.start();
	}
    }

    void playInternal(final int[] mixBuf, final byte[] outBuf, final SourceDataLine audioLine) {
	final int count = ModPlayer.this.getAudio(mixBuf);
	int outIdx = 0;
	for (int mixIdx = 0, mixEnd = count * 2; mixIdx < mixEnd; mixIdx++) {
	    int ampl = mixBuf[mixIdx];
	    if (ampl > 32767) {
		ampl = 32767;
	    }
	    if (ampl < -32768) {
		ampl = -32768;
	    }
	    outBuf[outIdx++] = (byte) (ampl >> 8);
	    outBuf[outIdx++] = (byte) ampl;
	}
	audioLine.write(outBuf, 0, outIdx);
    }

    @Override
    public void stopLoop() {
	this.playing = false;
    }

    int getAudio(final int[] mixBuf) {
	final int count = this.ibxm.getAudio(mixBuf);
	return count;
    }
}
